#ifndef INC_DELAY_H
#define INC_DELAY_H

#include "stm8s.h"

void delay_timer_init(void); // vytvoření funkce na inicializaci delaye
void delay_1_sec(uint32_t time_ms); // tvoření názvu na volání delaye

#endif