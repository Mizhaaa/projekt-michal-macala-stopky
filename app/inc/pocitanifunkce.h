#ifndef INC_SEG1_H
#define INC_SEG1_H

#include "stm8s.h"
 // funkce na odpočet jedlotlivých segmentů 
int8_t zobrazeniSekundy(int8_t sekundy);
int8_t zobrazeniSekundy2(int8_t sekundy2);
int8_t zobrazeniMinuty(int8_t minuty);
int8_t zobrazeniMinuty2(int8_t minuty2);


// funkce na volání jednotlivých čísel na jednotlivých 7 segmentech

void c0seg1(void);
void c1seg1(void);
void c2seg1(void);
void c3seg1(void);
void c4seg1(void);
void c5seg1(void);
void c6seg1(void);
void c7seg1(void);
void c8seg1(void);
void c9seg1(void);
void c0seg2(void);
void c1seg2(void);
void c2seg2(void);
void c3seg2(void);
void c4seg2(void);
void c5seg2(void);
void c6seg2(void);
void c7seg2(void);
void c8seg2(void);
void c9seg2(void);
void c0seg3(void);
void c1seg3(void);
void c2seg3(void);
void c3seg3(void);
void c4seg3(void);
void c5seg3(void);
void c6seg3(void);
void c7seg3(void);
void c8seg3(void);
void c9seg3(void);
void c0seg4(void);
void c1seg4(void);
void c2seg4(void);
void c3seg4(void);
void c4seg4(void);
void c5seg4(void);
void c6seg4(void);
void c7seg4(void);
void c8seg4(void);
void c9seg4(void);

#endif



