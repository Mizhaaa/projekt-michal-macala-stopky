#include "pocitanifunkce.h"
#include "zpozdeni_1_sekunda.h"

// využití funkcí vytvořených níže k vytvoření podmínek 


int8_t zobrazeniSekundy(int8_t sekundy) // zobrazuju sekundy na prvním segmentu
{
    if (sekundy == 0)
    {
        c0seg1();
    }

    else if (sekundy == 1)
    {
        c1seg1();
    } 

    else if (sekundy == 2)
    {
        c2seg1();
    }

    else if (sekundy == 3)
    {
        c3seg1();
    }

    else if (sekundy == 4)
    {
        c4seg1();
    }

    else if (sekundy == 5)
    {
        c5seg1();
    }

    else if (sekundy == 6)
    {
        c6seg1();
    }

    else if (sekundy == 7)
    {
        c7seg1();
    }

    else if (sekundy == 8)
    {
        c8seg1();
    }

    else if (sekundy == 9)
    {
        c9seg1();
    }

    return sekundy;
}

int8_t zobrazeniSekundy2(int8_t sekundy2) // zobrazuju sekundy na druhém segmentu
{
    if (sekundy2 == 0)
    {
        c0seg2();
    }    
    
    else if (sekundy2 == 1)
    {
        c1seg2();
    }
    else if (sekundy2 == 2)
    {
        c2seg2();
    }
    else if (sekundy2 == 3)
    {
        c3seg2();
    }

    else if (sekundy2 == 4)
    {
        c4seg2();
    }

    else if (sekundy2 == 5)
    {
        c5seg2();
    }

    return sekundy2;
}

int8_t zobrazeniMinuty(int8_t minuty) // zobrazení minut na třetím segmentu
{
    if (minuty == 0)
    {
        c0seg3();
    }
    else if (minuty == 1)
    {
        c1seg3();
    } 
    else if (minuty == 2)
    {
        c2seg3();
    }
    else if (minuty == 3)
    {
        c3seg3();
    }
    else if (minuty == 4)
    {
        c4seg3();
    }
    else if (minuty == 5)
    {
        c5seg3();
    }
    else if (minuty == 6)
    {
        c6seg3();
    }
    else if (minuty == 7)
    {
        c7seg3();
    }
    else if (minuty == 8)
    {
        c8seg3();
    }
    else if (minuty == 9)
    {
        c9seg3();
    }

    return minuty;
}

int8_t zobrazeniMinuty2(int8_t minuty2) // zobrazení minut na čtvrtém segmentu
{
    if (minuty2 == 0)
    {
        c0seg4();
    }
    else if (minuty2 == 1)
    {
        c1seg4();
    }
    else if (minuty2 == 2)
    {
        c2seg4();
    }
    else if (minuty2 == 3)
    {
        c3seg4();
    }
    else if (minuty2 == 4)
    {
        c4seg4();
    }
    else if (minuty2 == 5)
    {
        c5seg4();
    }
    
    return minuty2;
}


// přiřazování jednotlivých pinů na stmku k zobrazení jednotlivých čísel 0-9 na segmentu 1-4, mám tam navíc u druhého a čtvrtého segmentu funkce pro čísla 6-9, což není nutné, ale nechám to tam...

void c0seg1(void)
{
    GPIO_WriteLow(GPIOC, GPIO_PIN_1);
    GPIO_WriteLow(GPIOC, GPIO_PIN_2);
    GPIO_WriteLow(GPIOC, GPIO_PIN_3);
    GPIO_WriteLow(GPIOC, GPIO_PIN_4);
    GPIO_WriteLow(GPIOC, GPIO_PIN_5);
    GPIO_WriteLow(GPIOC, GPIO_PIN_6);
    GPIO_WriteHigh(GPIOC, GPIO_PIN_7);

}

void c1seg1(void)
{
    GPIO_WriteHigh(GPIOC, GPIO_PIN_1);
    GPIO_WriteLow(GPIOC, GPIO_PIN_2);
    GPIO_WriteLow(GPIOC, GPIO_PIN_3);
    GPIO_WriteHigh(GPIOC, GPIO_PIN_4);
    GPIO_WriteHigh(GPIOC, GPIO_PIN_5);
    GPIO_WriteHigh(GPIOC, GPIO_PIN_6);
    GPIO_WriteHigh(GPIOC, GPIO_PIN_7);
}

void c2seg1(void)
{
    GPIO_WriteLow(GPIOC, GPIO_PIN_1);
    GPIO_WriteLow(GPIOC, GPIO_PIN_2);
    GPIO_WriteHigh(GPIOC, GPIO_PIN_3);
    GPIO_WriteLow(GPIOC, GPIO_PIN_4);
    GPIO_WriteLow(GPIOC, GPIO_PIN_5);
    GPIO_WriteHigh(GPIOC, GPIO_PIN_6);
    GPIO_WriteLow(GPIOC, GPIO_PIN_7);


}

void c3seg1(void)
{
    GPIO_WriteLow(GPIOC, GPIO_PIN_1);
    GPIO_WriteLow(GPIOC, GPIO_PIN_2);
    GPIO_WriteLow(GPIOC, GPIO_PIN_3);
    GPIO_WriteLow(GPIOC, GPIO_PIN_4);
    GPIO_WriteHigh(GPIOC, GPIO_PIN_5);
    GPIO_WriteHigh(GPIOC, GPIO_PIN_6);
    GPIO_WriteLow(GPIOC, GPIO_PIN_7);

}

void c4seg1(void)
{
    GPIO_WriteHigh(GPIOC, GPIO_PIN_1);
    GPIO_WriteLow(GPIOC, GPIO_PIN_2);
    GPIO_WriteLow(GPIOC, GPIO_PIN_3);
    GPIO_WriteHigh(GPIOC, GPIO_PIN_4);
    GPIO_WriteHigh(GPIOC, GPIO_PIN_5);
    GPIO_WriteLow(GPIOC, GPIO_PIN_6);
    GPIO_WriteLow(GPIOC, GPIO_PIN_7);

}

void c5seg1(void)
{
    GPIO_WriteLow(GPIOC, GPIO_PIN_1);
    GPIO_WriteHigh(GPIOC, GPIO_PIN_2);
    GPIO_WriteLow(GPIOC, GPIO_PIN_3);
    GPIO_WriteLow(GPIOC, GPIO_PIN_4);
    GPIO_WriteHigh(GPIOC, GPIO_PIN_5);
    GPIO_WriteLow(GPIOC, GPIO_PIN_6);
    GPIO_WriteLow(GPIOC, GPIO_PIN_7);
}

void c6seg1(void)
{
    GPIO_WriteLow(GPIOC, GPIO_PIN_1);
    GPIO_WriteHigh(GPIOC, GPIO_PIN_2);
    GPIO_WriteLow(GPIOC, GPIO_PIN_3);
    GPIO_WriteLow(GPIOC, GPIO_PIN_4);
    GPIO_WriteLow(GPIOC, GPIO_PIN_5);
    GPIO_WriteLow(GPIOC, GPIO_PIN_6);
    GPIO_WriteLow(GPIOC, GPIO_PIN_7);

}


void c7seg1(void)
{
    GPIO_WriteLow(GPIOC, GPIO_PIN_1);
    GPIO_WriteLow(GPIOC, GPIO_PIN_2);
    GPIO_WriteLow(GPIOC, GPIO_PIN_3);
    GPIO_WriteHigh(GPIOC, GPIO_PIN_4);
    GPIO_WriteHigh(GPIOC, GPIO_PIN_5);
    GPIO_WriteHigh(GPIOC, GPIO_PIN_6);
    GPIO_WriteHigh(GPIOC, GPIO_PIN_7);

}

void c8seg1(void)
{
    GPIO_WriteLow(GPIOC, GPIO_PIN_1);
    GPIO_WriteLow(GPIOC, GPIO_PIN_2);
    GPIO_WriteLow(GPIOC, GPIO_PIN_3);
    GPIO_WriteLow(GPIOC, GPIO_PIN_4);
    GPIO_WriteLow(GPIOC, GPIO_PIN_5);
    GPIO_WriteLow(GPIOC, GPIO_PIN_6);
    GPIO_WriteLow(GPIOC, GPIO_PIN_7);

}

void c9seg1(void)
{
    GPIO_WriteLow(GPIOC, GPIO_PIN_1);
    GPIO_WriteLow(GPIOC, GPIO_PIN_2);
    GPIO_WriteLow(GPIOC, GPIO_PIN_3);
    GPIO_WriteLow(GPIOC, GPIO_PIN_4);
    GPIO_WriteHigh(GPIOC, GPIO_PIN_5);
    GPIO_WriteLow(GPIOC, GPIO_PIN_6);
    GPIO_WriteLow(GPIOC, GPIO_PIN_7);
}

void c0seg2(void)
{
    GPIO_WriteLow(GPIOE, GPIO_PIN_1);
    GPIO_WriteLow(GPIOD, GPIO_PIN_2);
    GPIO_WriteLow(GPIOD, GPIO_PIN_3);
    GPIO_WriteLow(GPIOD, GPIO_PIN_4);
    GPIO_WriteLow(GPIOD, GPIO_PIN_5);
    GPIO_WriteLow(GPIOG, GPIO_PIN_2);
    GPIO_WriteHigh(GPIOD, GPIO_PIN_7);
}
void c1seg2(void)
{
    GPIO_WriteHigh(GPIOE, GPIO_PIN_1);
    GPIO_WriteLow(GPIOD,  GPIO_PIN_2);
    GPIO_WriteLow(GPIOD,  GPIO_PIN_3);
    GPIO_WriteHigh(GPIOD, GPIO_PIN_4);
    GPIO_WriteHigh(GPIOD, GPIO_PIN_5);
    GPIO_WriteHigh(GPIOG, GPIO_PIN_2);
    GPIO_WriteHigh(GPIOD, GPIO_PIN_7);
}

void c2seg2(void)
{
    GPIO_WriteLow(GPIOE, GPIO_PIN_1);
    GPIO_WriteLow(GPIOD, GPIO_PIN_2);
    GPIO_WriteHigh(GPIOD, GPIO_PIN_3);
    GPIO_WriteLow(GPIOD, GPIO_PIN_4);
    GPIO_WriteLow(GPIOD, GPIO_PIN_5);
    GPIO_WriteHigh(GPIOG, GPIO_PIN_2);
    GPIO_WriteLow(GPIOD, GPIO_PIN_7);


}

void c3seg2(void)
{
    GPIO_WriteLow(GPIOE, GPIO_PIN_1);
    GPIO_WriteLow(GPIOD, GPIO_PIN_2);
    GPIO_WriteLow(GPIOD, GPIO_PIN_3);
    GPIO_WriteLow(GPIOD, GPIO_PIN_4);
    GPIO_WriteHigh(GPIOD, GPIO_PIN_5);
    GPIO_WriteHigh(GPIOG, GPIO_PIN_2);
    GPIO_WriteLow(GPIOD, GPIO_PIN_7);

}

void c4seg2(void)
{
    GPIO_WriteHigh(GPIOE, GPIO_PIN_1);
    GPIO_WriteLow(GPIOD, GPIO_PIN_2);
    GPIO_WriteLow(GPIOD, GPIO_PIN_3);
    GPIO_WriteHigh(GPIOD, GPIO_PIN_4);
    GPIO_WriteHigh(GPIOD, GPIO_PIN_5);
    GPIO_WriteLow(GPIOG, GPIO_PIN_2);
    GPIO_WriteLow(GPIOD, GPIO_PIN_7);

}

void c5seg2(void)
{
    GPIO_WriteLow(GPIOE, GPIO_PIN_1);
    GPIO_WriteHigh(GPIOD, GPIO_PIN_2);
    GPIO_WriteLow(GPIOD, GPIO_PIN_3);
    GPIO_WriteLow(GPIOD, GPIO_PIN_4);
    GPIO_WriteHigh(GPIOD, GPIO_PIN_5);
    GPIO_WriteLow(GPIOG, GPIO_PIN_2);
    GPIO_WriteLow(GPIOD, GPIO_PIN_7);
}

void c6seg2(void)
{
    GPIO_WriteLow(GPIOE, GPIO_PIN_1);
    GPIO_WriteHigh(GPIOD, GPIO_PIN_2);
    GPIO_WriteLow(GPIOD, GPIO_PIN_3);
    GPIO_WriteLow(GPIOD, GPIO_PIN_4);
    GPIO_WriteLow(GPIOD, GPIO_PIN_5);
    GPIO_WriteLow(GPIOG, GPIO_PIN_2);
    GPIO_WriteLow(GPIOD, GPIO_PIN_7);

}


void c7seg2(void)
{
    GPIO_WriteLow(GPIOE, GPIO_PIN_1);
    GPIO_WriteLow(GPIOD, GPIO_PIN_2);
    GPIO_WriteLow(GPIOD, GPIO_PIN_3);
    GPIO_WriteHigh(GPIOD, GPIO_PIN_4);
    GPIO_WriteHigh(GPIOD, GPIO_PIN_5);
    GPIO_WriteHigh(GPIOG, GPIO_PIN_2);
    GPIO_WriteHigh(GPIOD, GPIO_PIN_7);

}

void c8seg2(void)
{
    GPIO_WriteLow(GPIOE, GPIO_PIN_1);
    GPIO_WriteLow(GPIOD, GPIO_PIN_2);
    GPIO_WriteLow(GPIOD, GPIO_PIN_3);
    GPIO_WriteLow(GPIOD, GPIO_PIN_4);
    GPIO_WriteLow(GPIOD, GPIO_PIN_5);
    GPIO_WriteLow(GPIOG, GPIO_PIN_2);
    GPIO_WriteLow(GPIOD, GPIO_PIN_7);

}

void c9seg2(void)
{
    GPIO_WriteLow(GPIOE, GPIO_PIN_1);
    GPIO_WriteLow(GPIOD, GPIO_PIN_2);
    GPIO_WriteLow(GPIOD, GPIO_PIN_3);
    GPIO_WriteLow(GPIOD, GPIO_PIN_4);
    GPIO_WriteHigh(GPIOD, GPIO_PIN_5);
    GPIO_WriteLow(GPIOG, GPIO_PIN_2);
    GPIO_WriteLow(GPIOD, GPIO_PIN_7);
}

void c0seg3(void)
{
    GPIO_WriteLow(GPIOG, GPIO_PIN_4);
    GPIO_WriteLow(GPIOG, GPIO_PIN_5);
    GPIO_WriteLow(GPIOE, GPIO_PIN_3);
    GPIO_WriteLow(GPIOG, GPIO_PIN_1);
    GPIO_WriteLow(GPIOE, GPIO_PIN_5);
    GPIO_WriteLow(GPIOE, GPIO_PIN_6);
    GPIO_WriteHigh(GPIOE, GPIO_PIN_7);
    GPIO_WriteLow(GPIOE, GPIO_PIN_0);

}

void c1seg3(void)
{
    GPIO_WriteHigh(GPIOG, GPIO_PIN_4);
    GPIO_WriteLow(GPIOG, GPIO_PIN_5);
    GPIO_WriteLow(GPIOE, GPIO_PIN_3);
    GPIO_WriteHigh(GPIOG, GPIO_PIN_1);
    GPIO_WriteHigh(GPIOE, GPIO_PIN_5);
    GPIO_WriteHigh(GPIOE, GPIO_PIN_6);
    GPIO_WriteHigh(GPIOE, GPIO_PIN_7);
    GPIO_WriteLow(GPIOE, GPIO_PIN_0);
}

void c2seg3(void)
{
    GPIO_WriteLow(GPIOG, GPIO_PIN_4);
    GPIO_WriteLow(GPIOG, GPIO_PIN_5);
    GPIO_WriteHigh(GPIOE, GPIO_PIN_3);
    GPIO_WriteLow(GPIOG, GPIO_PIN_1);
    GPIO_WriteLow(GPIOE, GPIO_PIN_5);
    GPIO_WriteHigh(GPIOE, GPIO_PIN_6);
    GPIO_WriteLow(GPIOE, GPIO_PIN_7);
    GPIO_WriteLow(GPIOE, GPIO_PIN_0);


}

void c3seg3(void)
{
    GPIO_WriteLow(GPIOG, GPIO_PIN_4);
    GPIO_WriteLow(GPIOG, GPIO_PIN_5);
    GPIO_WriteLow(GPIOE, GPIO_PIN_3);
    GPIO_WriteLow(GPIOG, GPIO_PIN_1);
    GPIO_WriteHigh(GPIOE, GPIO_PIN_5);
    GPIO_WriteHigh(GPIOE, GPIO_PIN_6);
    GPIO_WriteLow(GPIOE, GPIO_PIN_7);
    GPIO_WriteLow(GPIOE, GPIO_PIN_0);

}

void c4seg3(void)
{
    GPIO_WriteHigh(GPIOG, GPIO_PIN_4);
    GPIO_WriteLow(GPIOG, GPIO_PIN_5);
    GPIO_WriteLow(GPIOE, GPIO_PIN_3);
    GPIO_WriteHigh(GPIOG, GPIO_PIN_1);
    GPIO_WriteHigh(GPIOE, GPIO_PIN_5);
    GPIO_WriteLow(GPIOE, GPIO_PIN_6);
    GPIO_WriteLow(GPIOE, GPIO_PIN_7);
    GPIO_WriteLow(GPIOE, GPIO_PIN_0);

}

void c5seg3(void)
{
    GPIO_WriteLow(GPIOG, GPIO_PIN_4);
    GPIO_WriteHigh(GPIOG, GPIO_PIN_5);
    GPIO_WriteLow(GPIOE, GPIO_PIN_3);
    GPIO_WriteLow(GPIOG, GPIO_PIN_1);
    GPIO_WriteHigh(GPIOE, GPIO_PIN_5);
    GPIO_WriteLow(GPIOE, GPIO_PIN_6);
    GPIO_WriteLow(GPIOE, GPIO_PIN_7);
    GPIO_WriteLow(GPIOE, GPIO_PIN_0);
}

void c6seg3(void)
{
    GPIO_WriteLow(GPIOG, GPIO_PIN_4);
    GPIO_WriteHigh(GPIOG, GPIO_PIN_5);
    GPIO_WriteLow(GPIOE, GPIO_PIN_3);
    GPIO_WriteLow(GPIOG, GPIO_PIN_1);
    GPIO_WriteLow(GPIOE, GPIO_PIN_5);
    GPIO_WriteLow(GPIOE, GPIO_PIN_6);
    GPIO_WriteLow(GPIOE, GPIO_PIN_7);
    GPIO_WriteLow(GPIOE, GPIO_PIN_0);

}


void c7seg3(void)
{
    GPIO_WriteLow(GPIOG, GPIO_PIN_4);
    GPIO_WriteLow(GPIOG, GPIO_PIN_5);
    GPIO_WriteLow(GPIOE, GPIO_PIN_3);
    GPIO_WriteHigh(GPIOG, GPIO_PIN_1);
    GPIO_WriteHigh(GPIOE, GPIO_PIN_5);
    GPIO_WriteHigh(GPIOE, GPIO_PIN_6);
    GPIO_WriteHigh(GPIOE, GPIO_PIN_7);
    GPIO_WriteLow(GPIOE, GPIO_PIN_0);

}

void c8seg3(void)
{
    GPIO_WriteLow(GPIOG, GPIO_PIN_4);
    GPIO_WriteLow(GPIOG, GPIO_PIN_5);
    GPIO_WriteLow(GPIOE, GPIO_PIN_3);
    GPIO_WriteLow(GPIOG, GPIO_PIN_1);
    GPIO_WriteLow(GPIOE, GPIO_PIN_5);
    GPIO_WriteLow(GPIOE, GPIO_PIN_6);
    GPIO_WriteLow(GPIOE, GPIO_PIN_7);
    GPIO_WriteLow(GPIOE, GPIO_PIN_0);

}

void c9seg3(void)
{
    GPIO_WriteLow(GPIOG, GPIO_PIN_4);
    GPIO_WriteLow(GPIOG, GPIO_PIN_5);
    GPIO_WriteLow(GPIOE, GPIO_PIN_3);
    GPIO_WriteLow(GPIOG, GPIO_PIN_1);
    GPIO_WriteHigh(GPIOE, GPIO_PIN_5);
    GPIO_WriteLow(GPIOE, GPIO_PIN_6);
    GPIO_WriteLow(GPIOE, GPIO_PIN_7);
    GPIO_WriteLow(GPIOE, GPIO_PIN_0);
} 

void c0seg4(void)
{
    GPIO_WriteLow(GPIOB, GPIO_PIN_1);
    GPIO_WriteLow(GPIOB, GPIO_PIN_2);
    GPIO_WriteLow(GPIOB, GPIO_PIN_3);
    GPIO_WriteLow(GPIOB, GPIO_PIN_4);
    GPIO_WriteLow(GPIOB, GPIO_PIN_5);
    GPIO_WriteLow(GPIOB, GPIO_PIN_6);
    GPIO_WriteHigh(GPIOB, GPIO_PIN_7);

}

void c1seg4(void)
{
    GPIO_WriteHigh(GPIOB, GPIO_PIN_1);
    GPIO_WriteLow(GPIOB, GPIO_PIN_2);
    GPIO_WriteLow(GPIOB, GPIO_PIN_3);
    GPIO_WriteHigh(GPIOB, GPIO_PIN_4);
    GPIO_WriteHigh(GPIOB, GPIO_PIN_5);
    GPIO_WriteHigh(GPIOB, GPIO_PIN_6);
    GPIO_WriteHigh(GPIOB, GPIO_PIN_7);
}

void c2seg4(void)
{
    GPIO_WriteLow(GPIOB, GPIO_PIN_1);
    GPIO_WriteLow(GPIOB, GPIO_PIN_2);
    GPIO_WriteHigh(GPIOB, GPIO_PIN_3);
    GPIO_WriteLow(GPIOB, GPIO_PIN_4);
    GPIO_WriteLow(GPIOB, GPIO_PIN_5);
    GPIO_WriteHigh(GPIOB, GPIO_PIN_6);
    GPIO_WriteLow(GPIOB, GPIO_PIN_7);


}

void c3seg4(void)
{
    GPIO_WriteLow(GPIOB, GPIO_PIN_1);
    GPIO_WriteLow(GPIOB, GPIO_PIN_2);
    GPIO_WriteLow(GPIOB, GPIO_PIN_3);
    GPIO_WriteLow(GPIOB, GPIO_PIN_4);
    GPIO_WriteHigh(GPIOB, GPIO_PIN_5);
    GPIO_WriteHigh(GPIOB, GPIO_PIN_6);
    GPIO_WriteLow(GPIOB, GPIO_PIN_7);

}

void c4seg4(void)
{
    GPIO_WriteHigh(GPIOB, GPIO_PIN_1);
    GPIO_WriteLow(GPIOB, GPIO_PIN_2);
    GPIO_WriteLow(GPIOB, GPIO_PIN_3);
    GPIO_WriteHigh(GPIOB, GPIO_PIN_4);
    GPIO_WriteHigh(GPIOB, GPIO_PIN_5);
    GPIO_WriteLow(GPIOB, GPIO_PIN_6);
    GPIO_WriteLow(GPIOB, GPIO_PIN_7);

}

void c5seg4(void)
{
    GPIO_WriteLow(GPIOB, GPIO_PIN_1);
    GPIO_WriteHigh(GPIOB, GPIO_PIN_2);
    GPIO_WriteLow(GPIOB, GPIO_PIN_3);
    GPIO_WriteLow(GPIOB, GPIO_PIN_4);
    GPIO_WriteHigh(GPIOB, GPIO_PIN_5);
    GPIO_WriteLow(GPIOB, GPIO_PIN_6);
    GPIO_WriteLow(GPIOB, GPIO_PIN_7);
}

void c6seg4(void)
{
    GPIO_WriteLow(GPIOB, GPIO_PIN_1);
    GPIO_WriteHigh(GPIOB, GPIO_PIN_2);
    GPIO_WriteLow(GPIOB, GPIO_PIN_3);
    GPIO_WriteLow(GPIOB, GPIO_PIN_4);
    GPIO_WriteLow(GPIOB, GPIO_PIN_5);
    GPIO_WriteLow(GPIOB, GPIO_PIN_6);
    GPIO_WriteLow(GPIOB, GPIO_PIN_7);

}


void c7seg4(void)
{
    GPIO_WriteLow(GPIOB, GPIO_PIN_1);
    GPIO_WriteLow(GPIOB, GPIO_PIN_2);
    GPIO_WriteLow(GPIOB, GPIO_PIN_3);
    GPIO_WriteHigh(GPIOB, GPIO_PIN_4);
    GPIO_WriteHigh(GPIOB, GPIO_PIN_5);
    GPIO_WriteHigh(GPIOB, GPIO_PIN_6);
    GPIO_WriteHigh(GPIOB, GPIO_PIN_7);

}

void c8seg4(void)
{
    GPIO_WriteLow(GPIOB, GPIO_PIN_1);
    GPIO_WriteLow(GPIOB, GPIO_PIN_2);
    GPIO_WriteLow(GPIOB, GPIO_PIN_3);
    GPIO_WriteLow(GPIOB, GPIO_PIN_4);
    GPIO_WriteLow(GPIOB, GPIO_PIN_5);
    GPIO_WriteLow(GPIOB, GPIO_PIN_6);
    GPIO_WriteLow(GPIOB, GPIO_PIN_7);

}

void c9seg4(void)
{
    GPIO_WriteLow(GPIOB, GPIO_PIN_1);
    GPIO_WriteLow(GPIOB, GPIO_PIN_2);
    GPIO_WriteLow(GPIOB, GPIO_PIN_3);
    GPIO_WriteLow(GPIOB, GPIO_PIN_4);
    GPIO_WriteHigh(GPIOB, GPIO_PIN_5);
    GPIO_WriteLow(GPIOB, GPIO_PIN_6);
    GPIO_WriteLow(GPIOB, GPIO_PIN_7);
}