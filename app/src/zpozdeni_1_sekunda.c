#include "zpozdeni_1_sekunda.h"
// použitý funkce na delay z jedné z hodin mitka
void delay_timer_init(void)   // funkce na inicializaci timeru
{
    TIM3_TimeBaseInit(TIM3_PRESCALER_256, 62499);   // inicializace timeru 3 a určení předděličky na hodnotu 256 a stropu čítače 62499
    TIM3_Cmd(ENABLE); // povolení timeru
}

void delay_1_sec(uint32_t time_ms) // funkce na delay jedné sekundy
{
    TIM3_SetCounter(0); // nastavíme časovač na hodnotu 0 sekund
    for(uint32_t ms = 0; ms < time_ms; ms++)
    {
        while(TIM3_GetFlagStatus(TIM3_FLAG_UPDATE) != SET) // kontrolujeme stav vlajky
        {
            tlacitko1(); // tlacitko1 = funkce pro start a stop "aka" první tlačítko
        }
        TIM3_ClearFlag(TIM3_FLAG_UPDATE); // smazání / vyčištění vlajky čítače
    }
}