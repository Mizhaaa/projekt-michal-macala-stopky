//přidání funkcí na počítání a delay z ostatních souborů
#include "stm8s.h"
#include "zpozdeni_1_sekunda.h"
#include "pocitanifunkce.h"

int8_t stop = 1; // vytvoření 8 bitové znaménkové proměnné s kterou potom budeme dál pracovat

int8_t stopButtonIsPressed(void)
{
    return (GPIO_ReadInputPin(GPIOE, GPIO_PIN_4) == 0); // snímání / čtení pinu E č,4 (start/stop tlačítko), nastaven na log nulu / připojen na zem
}

int8_t resetButtonIsPressed(void)
{
    return (GPIO_ReadInputPin(GPIOA, GPIO_PIN_5) == 0); // snímání / čtení pinu A č 5, (RESET), nasteven na log nulu / připojen na zem
}

int resetA(int8_t sekundy) // funkce pro reset prvního segmentu
{
    if (resetButtonIsPressed())
    {
        sekundy = -1;    // dal jsem na -1, protože když bylo na nule, tak nula tam byla pouze krátkou chvilku a nedělalo to dobrý dojem, vypadalo to, že to začíná jedničkou
        zobrazeniSekundy(sekundy);
    }

    return sekundy;
}

int resetB(int8_t sekundy2) // funkce pro reset druhého segmentu
{
    if (resetButtonIsPressed())
    {
        sekundy2 = 0;
        zobrazeniSekundy2(sekundy2);
    }

    return sekundy2;
}

int resetC(int8_t minuty) // funkce pro reset třetí segmentu
{
    if (resetButtonIsPressed())
    {
        minuty = 0;
        zobrazeniMinuty(minuty);
    }

    return minuty;
}

int resetD(int8_t minuty2) // funkce pro reset čtvrého segmentu
{
    if (resetButtonIsPressed())
    {
        minuty2 = 0;
        zobrazeniMinuty2(minuty2);
    }

    return minuty2;
}

int tlacitko1(void) // funkce na stop a start 
{
    if (stopButtonIsPressed()) // pokud stisknu tlačítko č.1 
    {
        if (stop == 1) // pokud jsou stopky, stopnuté
        {
            stop = 0; // pokračování v čítání
        }
        else // pokud stopky běží
        {
            stop = 1; // stopky se zataví 
        }
        while (stopButtonIsPressed());
    }
    if (stop == 1)  // zajistí zastavení stopek
    { 
        while (!stopButtonIsPressed())
        {
        }
    }
}

void main(void)
{
    //inicializaci potřebných pinů na 7 segment a dvě tlačítka START/STOP, RESET
    CLK_HSIPrescalerConfig(CLK_PRESCALER_HSIDIV1); // FREQ MCU 16MHz
    GPIO_Init(GPIOD, GPIO_PIN_ALL, GPIO_MODE_OUT_PP_HIGH_SLOW);
    GPIO_Init(GPIOE, GPIO_PIN_ALL, GPIO_MODE_OUT_PP_HIGH_SLOW);
    GPIO_Init(GPIOC, GPIO_PIN_ALL, GPIO_MODE_OUT_PP_HIGH_SLOW);
    GPIO_Init(GPIOB, GPIO_PIN_ALL, GPIO_MODE_OUT_PP_HIGH_SLOW);
    GPIO_Init(GPIOG, GPIO_PIN_ALL, GPIO_MODE_OUT_PP_HIGH_SLOW);
    GPIO_Init(GPIOA, GPIO_PIN_ALL, GPIO_MODE_OUT_PP_HIGH_SLOW);
    GPIO_Init(GPIOE, GPIO_PIN_4, GPIO_MODE_IN_FL_NO_IT); //Tlačítko start/sop
    GPIO_Init(GPIOA, GPIO_PIN_5, GPIO_MODE_IN_FL_NO_IT); //Tlačítko reset
    delay_timer_init();


    // nastavení počátačeních hodnot od kterých program začne počítat,
    int8_t sekundy = 0;
    int8_t sekundy2 = 0;
    int8_t minuty = 0;
    int8_t minuty2 = 0;

    // použité funkce z pocitanifunkce.c na zobrazení čísel
    zobrazeniSekundy(sekundy);
    zobrazeniSekundy2(sekundy2);
    zobrazeniMinuty(minuty);
    zobrazeniMinuty2(minuty2);
    delay_1_sec(1);



    // hlavní while loop
    while (1)
    {
        sekundy = resetA(sekundy); // přiřazení hodnoty do proměnné, pokud zmáčkneme reset
        sekundy2 = resetB(sekundy2);
        minuty = resetC(minuty);
        minuty2 = resetD(minuty2);
        sekundy = sekundy + 1;
        zobrazeniSekundy(sekundy); // znovu použitá funkce na zobrazení segmentů
        zobrazeniSekundy2(sekundy2);
        zobrazeniMinuty(minuty);
        zobrazeniMinuty2(minuty2);
        delay_1_sec(1);

        if ((sekundy2 < 6) & (sekundy == 9)) // podmínka, která nám zajistí, že když na prvním display bude č. 9, tak se první display "vyresetuje" a přidá jednu sekundu na druhý segment
        {
            sekundy = -1;
            sekundy2 = sekundy2 + 1;
            zobrazeniSekundy(sekundy);
            zobrazeniSekundy2(sekundy2);
            zobrazeniMinuty(minuty);
            zobrazeniMinuty2(minuty2);
        }
        
       if (sekundy2 == 6) // pokud druhý segment je roven 6 / první dva segmenty dosáhli 60 sekund, takže musíme segmenty vynulovat a přidat jednu minutu

        {
            sekundy2 = 0;
            minuty = minuty + 1;
            zobrazeniSekundy(sekundy);
            zobrazeniSekundy2(sekundy2);
            zobrazeniMinuty(minuty);
            zobrazeniMinuty2(minuty2);
        }
       
       if (minuty > 9) // pokud nám třetí segment dosáhne devítí minut tak se na čtvrtý segment přidá 10 minut a devítka se vynuluje.
        {
            minuty = 0;
            minuty2 = minuty2 + 1;
            zobrazeniSekundy(sekundy);
            zobrazeniSekundy2(sekundy2);
            zobrazeniMinuty(minuty);
            zobrazeniMinuty2(minuty2);
        }

        if (minuty2 == 6) // pokud poslední segment dosáhné hodnoty 6, tak se stopky automaticky vynulují na defaultní hodnoty
        {
            sekundy = 0;
            sekundy2 = 0;
            minuty = 0;
            minuty2 = 0;
            stop = 1;
            zobrazeniSekundy(sekundy);
            zobrazeniSekundy2(sekundy2);
            zobrazeniMinuty(minuty);
            zobrazeniMinuty2(minuty2);
            tlacitko1();
            sekundy = -1;
        }
    }
}