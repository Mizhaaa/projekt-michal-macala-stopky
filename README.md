# Stopky - Michal Máčala 3.B


## Úvod

Dobrý den, vítám vás v dokumentaci mého závěřečného projektu ve třetím ročníku.
Z názvu jste jistě mohli poznat, že se jedná o stopky, které jsem realizoval pomocí  nepájivého pole, stmka a pár součástek, které jsem uvedl v tabulce.
***

## Funkce

Projekt funguje jako klasické stopky. Stisknutím tlačítka č.1 "START/STOP" se spustí odpočet na sedmi segmentech až do 59:59. Samozřejmě můžeme čas kdykoliv pozastavit tím stejným tlačítkem. Dále zde máme druhé tlačítko "RESET", díky kterému můžeme stopky vynulovat.
***
## Tabulka součástek

| Název Součástky | Hodnota | Počet kusů | Odkaz na eshop a cena|
| :-------------- |:-----:    |:--------:     | :----:
| Rezistory       |330Ω      | 29           | [KLIK](https://www.gme.cz/rm-330r-0207-0-6w-1#) - 87kč
| 7 Segment display|     -    | 4         | [KLIK](https://www.soselectronic.cz/products/kingbright/sa-56-11-ewa-12362#) - 98kč
| Tlačítko| -| 2| [KLIK](https://www.hwkitchen.cz/spinaci-tlacitko-do-kontaktniho-pole/#) - 14kč
| Cena celkem bez stmka| -| -|   199kč

***




## Schéma a fotky projektu

1. Fotka zapojení v reálném životě

![](foto1.JPG)
***
2. Blokové schéma

![](foto3.JPG)
***
3. Schéma zapojení v kicadu

![](foto2.JPG)
***


## Závěr projektu
Na závěr bych jenom rád řekl, že podle mého názoru se mi projekt relativně vydařil, ale kdybych ho měl dělat znovu tak bych možná zvážil realizaci pomocí dekodérů, hlavně kvůli značně menšímu počtu vodičů.
Také jsem narazil na pár nepříjemných problémů s nepájivým polem, kde jsem narazil na studený spoj a musel jsem celý obvod přesunout na druhou stranu pole (2x) a věřte mi, jednoduché to zrovna nebylo :).

Kód jsem taky hodněkrát přepisoval a předělával, poslední komplikací byl probíjející pin na STMKU :).

)
***

